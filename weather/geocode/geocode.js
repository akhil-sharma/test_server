const request = require('request');
const path = require('path');

const geocode_api_key = require(path.join(__dirname, '..', '..', '/api_keys')).geocode_api_key;

var geocodeAddressPromise = (encodedAddress) => {
    return new Promise((resolve, reject) => {
        request({
            url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${geocode_api_key}`,
            json:true
            }, (error, response, body) => {
                if (error){
                    reject("Unable to connect to the servers...");
                } else if (body.status === 'ZERO_RESULTS'){
                    reject("Unable to find the given address...");
                }else if (body.status === 'OK'){
                    resolve({
                        address: body.results[0].formatted_address,
                        latitude: body.results[0].geometry.location.lat,
                        longitude: body.results[0].geometry.location.lng
                    });
                }
            
        });
    });

    
}

module.exports = {
    geocodeAddressPromise
}