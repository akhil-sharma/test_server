const request = require('request');
const path = require('path');


const geocode_api_key = require(path.join(__dirname, '..', '..', '/api_keys')).dark_sky_api_key;


var dark_sky_weather_promise = (latitude,longitude, callback) => {
    return new Promise((resolve, reject) => {
        request({
            url: `https://api.darksky.net/forecast/${dark_sky_api_key}/${latitude},${longitude}?units=si`,
            json:true
            }, (error, response, body) => {
                if (!error && response.statusCode === 200){
                    resolve({
                        temperature: body.currently.temperature,
                        apparentTemperature: body.currently.apparentTemperature,
                        summary: body.currently.summary
                    });
                }else{
                    reject('Unable to fetch the weather.')
                }
            });
    });
}

module.exports = {
    dark_sky_weather_promise
}