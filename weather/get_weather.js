const geocode = require('./geocode/geocode');
const dark_sky = require('./dark_sky/dark_sky');


//USING THE PROMISE BASED FUNCTIONS


var getWeatherJson = (address) => {
    geocode.geocodeAddressPromise(encodeURIComponent(address)).then((geocodeResult)=>{
        console.log(geocodeResult);
        return dark_sky.dark_sky_weather_promise(geocodeResult.latitude, geocodeResult.longitude);
    }).then((dark_sky_weather_result) => {
        console.log(dark_sky_weather_result);
        return {status: "Done", data: dark_sky_weather_result};
    }).catch((errorMessage) => {
        console.log(errorMessage);
        return {status: "Error" , data: {}}
    });
}

var getWeather = function * (address){
    var weather_data = yield getWeatherJson(address);
    console.log(weather_data);
}

module.exports = {
    getWeatherJson,
    getWeather
}