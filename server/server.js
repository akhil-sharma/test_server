const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const path = require('path');

const PORT = require('../constants').PORT;
const views_directory = path.join(__dirname, "../views");
const get_weather = require('../weather/get_weather');

var app = express();
/**
 * Configuring the app to use hbd for templating
 */
hbs.registerPartials(path.join(__dirname, '../partials'));
hbs.registerHelper('currentYear', function () {
	return new Date().getFullYear();
});
app.set('view engine', 'hbs');

/**
 * Middlewre for logging *
 */
app.use((req, res, next) => {
	let now = new Date().toString();
	let log = `${now}: ${req.method} ${req.url}\n`;
	fs.appendFileSync('server.log', log);
	next();
});

app.use(express.static(path.join(__dirname, "../public")))

app.get('/', (req, res) =>{
	res.render('home.hbs', {
		pageHeading: "Welcome!"
	});
});

/**
 * Handling get requests to /about
 */
app.get('/about', (req, res) => {
	res.render('about.hbs', {
		pageHeading: "Pleased to meet you!"
	})
})

/**
 * Handling get requests to /weather
 */
app.get('/weather', (req, res) => {
	// res.render('about.hbs', {
	// 	pageHeading: "Pleased to meet you!"
	// })
	get_weather.getWeather(160020);
})

/**
 * Handling get requests to /help
 */
app.get('/help', (req, res) => {
	res.render('help.hbs', {
		pageHeading: "Fret not!"
	})
})


app.listen(PORT, () => {
	console.log(`Listening on port ${PORT}. Started: ${new Date().getTime()}`);
});



